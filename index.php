<?php

// Release 0

require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("Shaun");

echo "Name : " . $sheep->name . "<br>"; //"shaun"
echo "Legs : " . $sheep->legs . "<br>"; //4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; //"no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
 

// Release 1

$kodok = new Frog("Buduk");

echo "Name : " . $kodok->name . "<br>"; //"buduk"
echo "Legs : " . $kodok->legs . "<br>"; //4
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; //"no"
$kodok->jump();//"Hop Hop"
echo "<br><br>"; 

$sungokong = new Ape("Kera Sakti");

echo "Name : " . $sungokong->name . "<br>"; //"kera sakti"
echo "Legs : " . $sungokong->legs . "<br>"; //2
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; //"no"
$sungokong->yell();//"Auooo"
echo "<br><br>"; 






?>